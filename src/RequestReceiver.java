import com.oocourse.elevator2.ElevatorInput;
import com.oocourse.elevator2.PersonRequest;

public class RequestReceiver implements Runnable {

    private Controller controller;

    public RequestReceiver() {
        controller = Controller.getController();
    }

    public void run() {
        ElevatorInput elevatorInput = new ElevatorInput(System.in);
        while (true) {
            PersonRequest request = elevatorInput.nextPersonRequest();
            // when request == null
            // it means there are no more lines in stdin
            if (request == null) {
                controller.addList(new PersonRequestBox(request, true));
                synchronized (controller) {
                    controller.notifyAll();
                }
                break;
            } else {
                // a new valid request
                //System.out.println(request);
                controller.addList(new PersonRequestBox(request, false));
                //TimableOutput.println(String.format("[input]%d-FROM-%d-TO-%d",
                // request.getPersonId(),request.getFromFloor(),
                // request.getToFloor()));
                synchronized (controller) {
                    controller.notifyAll();
                }
            }
        }
        try {
            elevatorInput.close();
        } catch (Exception e) {
            int nothing = 0;
        }
    }
}
