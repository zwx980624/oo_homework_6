import com.oocourse.TimableOutput;

public class Main {
    public static void main(String[] args)
    {
        TimableOutput.initStartTimestamp();
        new Thread(new RequestReceiver()).start();
        new Thread(new Elevator()).start();
    }
}
