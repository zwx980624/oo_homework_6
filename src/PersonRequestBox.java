import com.oocourse.elevator2.PersonRequest;

public class PersonRequestBox {
    private PersonRequest request;
    private boolean empty;

    public PersonRequestBox(PersonRequest r, boolean e) {
        request = r;
        empty = e;
    }

    public PersonRequest getRequest() {
        return request;
    }

    public boolean isEmpty() {
        return empty;
    }

    public int getPersonId() {
        return request.getPersonId();
    }

    public int getFromFloor() {
        return request.getFromFloor();
    }

    public int getToFloor() {
        return request.getToFloor();
    }
}