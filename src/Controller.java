import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class Controller {
    private Vector<PersonRequestBox> requestList;

    private static Controller controller = new Controller();

    private Controller() {
        requestList = new Vector<>();
    }

    public static synchronized Controller getController() {
        return controller;
    }

    public synchronized boolean isPickable(int floor, boolean direction) {
        for (PersonRequestBox t : requestList) {
            if (!t.isEmpty() && t.getFromFloor() == floor &&
                    direction == (t.getToFloor() > floor)) {
                return true;
            }
        }
        return false;
    }

    public synchronized List<PersonRequestBox>
                getPopPickList(int floor, boolean direction) {
        LinkedList<PersonRequestBox> ret = new LinkedList<>();
        Iterator<PersonRequestBox> it = requestList.iterator();
        while (it.hasNext()) {
            PersonRequestBox t = it.next();
            if (!t.isEmpty() && t.getFromFloor() == floor &&
                    direction == (t.getToFloor() > floor)) {
                it.remove();
                ret.add(t);
            }
        }
        return ret;
    }

    public synchronized void addList(PersonRequestBox pb) {
        requestList.add(pb);
    }

    public synchronized PersonRequestBox getMainRequest() {
        if (requestList.size() == 0) {
            return null;
        } else {
            PersonRequestBox req = requestList.get(0);
            requestList.remove(0);
            return req;
        }
    }

    private synchronized void popList(int index) {
        requestList.remove(index);
    }

    private synchronized void popList(PersonRequestBox pb) {
        requestList.remove(pb);
    }
}