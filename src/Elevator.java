import com.oocourse.TimableOutput;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Elevator implements Runnable {
    private int floor;
    private boolean direction;
    private static final double oneFloorTime = 0.4;
    private static final double openDoorTime = 0.2;
    private static final double closeDoorTime = 0.2;
    private Controller controller;
    private int state;
    private PersonRequestBox mainRequest;
    private List<PersonRequestBox> elevatorList;

    public Elevator() {
        floor = 1;
        controller = Controller.getController();
        state = 1;
        elevatorList = new LinkedList<>();
        direction = false;
    }

    public void run() {
        while (true) {
            try {
                if (state == 0) { break; }
                if (state == 1) {
                    while (true) {
                        mainRequest = controller.getMainRequest();
                        if (mainRequest == null) // empty list
                        {
                            synchronized (controller) { controller.wait(); }
                        } else {
                            if (mainRequest.isEmpty()) {
                                state = 0;
                                break;
                            }
                            moveToFloor(mainRequest.getFromFloor());
                            if (mainRequest.getToFloor() > floor) {
                                direction = true;
                                state = 2; //go up
                                floorProc(); //搞定初始层
                            } else {
                                direction = false;
                                state = 3; //go down
                                floorProc(); //搞定初始层
                            }
                            break;
                        }
                    }
                } else if (state == 2) //go up
                {
                    while (true) {
                        moveUp(); //初始层已搞定直接上走
                        while (floor != mainRequest.getToFloor()) {
                            if (needOpen()) { floorProc(); }
                            moveUp();
                        } //arrive to floor
                        floorProc();
                        mainRequest = getPopMainRequest(); //from elevator_list
                        if (mainRequest == null) //elevator empty
                        {
                            state = 1;
                            break;
                        }
                    }
                } else //go down
                {
                    while (true) {
                        moveDown(); //初始层已搞定直接上走
                        while (floor != mainRequest.getToFloor()) {
                            if (needOpen()) { floorProc(); }
                            moveDown();
                        } //arrive to floor
                        floorProc();
                        mainRequest = getPopMainRequest(); //from elevator_list
                        if (mainRequest == null) //elevator empty
                        {
                            state = 1;
                            break;
                        }
                    }
                }
            } catch (InterruptedException e) { int nothing = 0; }
        }
    }

    private boolean needOpen() //out: main_rq elv_list; in: main_rq ctrl
    {
        if (floor == mainRequest.getFromFloor() ||
                floor == mainRequest.getToFloor()) {
            return true;
        }
        for (PersonRequestBox r : elevatorList) {
            if (floor == r.getToFloor()) {
                return true;
            }
        }
        if (controller.isPickable(floor, direction)) {
            return true;
        }
        return false;
    }

    private PersonRequestBox getPopMainRequest() {
        if (elevatorList.size() == 0) {
            return null;
        } else {
            PersonRequestBox req = elevatorList.get(0);
            elevatorList.remove(0);
            return req;
        }
    }

    private void floorProc() throws InterruptedException {
        doorOpen();
        if (floor == mainRequest.getToFloor()) {
            mainRequestPersonOut();
        }
        personListOut();
        if (floor == mainRequest.getFromFloor()) {
            mainRequestPersonIn();
        }
        doorCheckClose();
    }

    private void doorCheckClose() throws InterruptedException {
        Thread.sleep((long) (closeDoorTime * 1000));
        List<PersonRequestBox> temp =
                controller.getPopPickList(floor, direction);
        elevatorList.addAll(temp);
        for (PersonRequestBox t : temp) {
            personIn(t.getPersonId());
        }
        TimableOutput.println(String.format("CLOSE-%d", floor));
    }

    private void personListOut() {
        Iterator<PersonRequestBox> it = elevatorList.iterator();
        while (it.hasNext()) {
            PersonRequestBox temp = it.next();
            if (temp.getToFloor() == floor) {
                it.remove();
                personOut(temp.getPersonId());
            }
        }
    }

    private void mainRequestPersonOut() {
        personOut(mainRequest.getPersonId());
    }

    private void mainRequestPersonIn() {
        personIn(mainRequest.getPersonId());
    }

    private void moveToFloor(int floorTo) throws InterruptedException {
        while (floorTo != floor) {
            if (floorTo > floor) {
                moveUp();
            } else {
                moveDown();
            }
        }
    }

    private void moveUp() throws InterruptedException {
        Thread.sleep((long) (oneFloorTime * 1000));
        if (floor != -1) {
            floor += 1;
        } else {
            floor = 1;
        }
        TimableOutput.println(String.format("ARRIVE-%d", floor));
    }

    private void moveDown() throws InterruptedException {
        Thread.sleep((long) (oneFloorTime * 1000));
        if (floor != 1) {
            floor -= 1;
        } else {
            floor = -1;
        }
        TimableOutput.println(String.format("ARRIVE-%d", floor));
    }

    private void doorOpen() throws InterruptedException {
        TimableOutput.println(String.format("OPEN-%d", floor));
        Thread.sleep((long) (openDoorTime * 1000));
    }

    private void doorClose() throws InterruptedException {
        Thread.sleep((long) (closeDoorTime * 1000));
        TimableOutput.println(String.format("CLOSE-%d", floor));
    }

    private void personIn(int id) {
        TimableOutput.println(String.format("IN-%d-%d", id, floor));
    }

    private void personOut(int id) {
        TimableOutput.println(String.format("OUT-%d-%d", id, floor));
    }
}
